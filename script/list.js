/* Запрос списка книг с сервера*/

document.addEventListener("DOMContentLoaded", sendRequest);

function sendRequest(event) {

    var loader = document.getElementById('loader');
    loader.style.display = 'block';

    var request = new XMLHttpRequest();

    request.open('GET', 'https://netology-fbb-store-api.herokuapp.com/book');

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            parsedArr = JSON.parse(request.responseText);
            createBook(parsedArr, 0, 4);
            loader.style.display = 'none';
            return parsedArr;
        }
    };
    request.send();  
};

/* Вывод книг по 4 */

function createBook(answer, start, finish) {        

    var booksList = document.getElementById('books');
    var moreButton = document.getElementsByClassName('more-books')[0];
    
    if (moreButton != null) {
        moreButton.parentNode.removeChild(moreButton);
    };
    

    for (i = start; i < finish; i++) {

        var newBook = document.createElement('figure');
        newBook.className = 'single-book';
        booksList.appendChild(newBook);

        var bookLink = document.createElement('a');
        bookLink.className = 'book-link';
        bookLink.setAttribute("href", "pages/book.html?" + answer[i].id)
        newBook.appendChild(bookLink);
		
        var bSmallCover = document.createElement('img');
        bSmallCover.className = 'book-cover-small';
        bSmallCover.src = answer[i].cover.small;
        bookLink.appendChild(bSmallCover);		
		
        var bookLinkHover = document.createElement('a');
        bookLinkHover.innerHTML = answer[i].title;
		bookLinkHover.className = 'hover';
        bookLinkHover.setAttribute("href", "pages/book.html?" + answer[i].id)
        bookLink.appendChild(bookLinkHover);		

        var bInfo = document.createElement('figcaption');
        bInfo.innerHTML = answer[i].info;   
        newBook.appendChild(bInfo);
    
    };
    
    start += 4;
    finish += 4; 
    
    if (finish < answer.length) {
        
        var moreButton = document.createElement('button');
        moreButton.className = 'more-books';
        moreButton.type = 'submit';
        moreButton.innerHTML = 'Ещё несколько книг';
        booksList.parentNode.appendChild(moreButton);        
        moreButton.addEventListener('click', function(){createBook(parsedArr, start, finish)});
        
    } else {

        finish = answer.length;
        var moreButton = document.createElement('button');
        moreButton.className = 'more-books';
        moreButton.type = 'submit';
        moreButton.innerHTML = 'Ещё несколько книг';
        booksList.parentNode.appendChild(moreButton);         
        moreButton.addEventListener('click', function(){createBook(parsedArr, start, finish)});
    };
    
    if (start >= answer.length) {
        moreButton.parentNode.removeChild(moreButton);
    };                                  
};

