/* Глазище */

document.onmousemove=function(event){
    
    var eye = document.getElementById("eye");
    var centerEyeX = (eye.getBoundingClientRect().right-eye.getBoundingClientRect().left)/2+eye.getBoundingClientRect().left;
    var centerEyeY = (eye.getBoundingClientRect().bottom-eye.getBoundingClientRect().top)/2+eye.getBoundingClientRect().top;
    var widthScreen = document.documentElement.clientWidth;
    var heightScreen = document.documentElement.clientHeight;
    var sizeEye = 53;
    
    if (event.pageY-window.pageYOffset > heightScreen) {
        var placeMouseY = heightScreen;
    } else {
        var placeMouseY = event.pageY-window.pageYOffset;
    }
    if (event.pageX > widthScreen) {
        var placeMouseX = widthScreen;
    } else{
        var placeMouseX = event.pageX;
    }
    
    var coordinateX = placeMouseX-centerEyeX;
    var coordinateY = placeMouseY-centerEyeY;
    
    if (coordinateX > 0 && coordinateY > 0) {
        var proportionX = coordinateX/(coordinateX+coordinateY);
        var proportionY = coordinateY/(coordinateX+coordinateY);
        var moveX = sizeEye/(widthScreen-centerEyeX);
        var moveY = sizeEye/(heightScreen-centerEyeY);
        var top = Math.round(coordinateY*moveY);
        var left = Math.round(coordinateX*moveX);
        if (top + left > 53) {
            var sum = top+left-53;
            top -= proportionX*sum;
            left -= proportionY*sum;
        }
        
    } else if (coordinateX < 0 && coordinateY < 0) {
        var proportionX = coordinateX/(coordinateX+coordinateY);
        var proportionY = coordinateY/(coordinateX+coordinateY);
        var moveX = sizeEye/centerEyeX;
        var moveY = sizeEye/centerEyeY;
        var top = Math.round(coordinateY*moveY);
        var left = Math.round(coordinateX*moveX);
        if (top+left < -53) {
            var sum=top+left+53;
            top += -proportionX*sum;
            left += -proportionY*sum;
        }
        
    } else if (coordinateX > 0 && coordinateY < 0){
        var proportionX = coordinateX/(coordinateX-coordinateY);
        var proportionY = -coordinateY/(coordinateX-coordinateY);
        var moveX = sizeEye/(widthScreen-centerEyeX);
        var moveY = sizeEye/centerEyeY;
        var top = Math.round(coordinateY*moveY);
        var left = Math.round(coordinateX*moveX);
        if (left-top > 53) {
            var sum = left-top-53;
            top += proportionX*sum;
            left -= proportionY*sum;
        }
        
    } else {
        var proportionX =- coordinateX/(coordinateY-coordinateX);
        var proportionY = coordinateY/(coordinateY-coordinateX);
        var moveX = sizeEye/centerEyeX;
        var moveY = sizeEye/(heightScreen-centerEyeY);
        var top = Math.round(coordinateY*moveY);
        var left = Math.round(coordinateX*moveX);
        if (top-left > 53) {
            var sum = top-left-53;
            top -= proportionX*sum;
            left += proportionY*sum;
        }
    }
    var top = top+58;
    var left = left+58;
    eye.firstElementChild.style.top = top+"px";
    eye.firstElementChild.style.left = left+"px";
};
