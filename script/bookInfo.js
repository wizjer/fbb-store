var windowSize = window.innerWidth;
var deliveryId, paymentId;

/* Сортировка блоков для разных разрешений */
document.addEventListener("DOMContentLoaded", sort);

function sort() {
    var bookStore = document.getElementById('bookStore');
    var bookInfo = document.getElementById('book-display');
    var buyBook = document.getElementById('buy-book');
    var bookReview = document.getElementById('reviews');    

    if (windowSize <= '640') {        
        bookStore.insertBefore(bookInfo, bookReview);
    };
};   

/* Запрос информации о книге */
document.addEventListener("DOMContentLoaded", getBook);

function getBook(event) {

var loader = document.getElementById('loader');
loader.style.display = 'block';

var bookIndex = window.location.search.slice(1);

var request = new XMLHttpRequest();

request.open('GET', 'https://netology-fbb-store-api.herokuapp.com/book/'+bookIndex);

request.onreadystatechange = function () {
  if (this.readyState === 4) {
      
    parsedObject = JSON.parse(this.responseText);    
	createBook(parsedObject);
      
	loader.style.display = 'none'; 
    return parsedObject;  
  }
};

request.send(); 
};


/* Запрос способов доставки */
document.addEventListener("DOMContentLoaded", getDeliveryList);

function getDeliveryList() {
    var requestDelivery = new XMLHttpRequest();

    requestDelivery.open('GET', 'https://netology-fbb-store-api.herokuapp.com/order/delivery');

    requestDelivery.onreadystatechange = function () {
      if (this.readyState === 4) {
          
        parsedDelivery = JSON.parse(this.responseText);
        return parsedDelivery; 
      }
    };

    requestDelivery.send();
};

/* Вывод книги */
function createBook(answer) {

		/*По порядку: ревью*/
		var reviews = document.getElementsByClassName('reviews')[0]; 

		parsedObject.reviews.forEach(function(item) {
			var review = document.createElement('figure');
			review.className = 'review';
			reviews.appendChild(review);

			var authorFace = document.createElement('img');
			authorFace.src = item.author.pic;
			authorFace.title = item.author.name;
			review.appendChild(authorFace);				
			
			var authorTalk = document.createElement('figcaption');
			authorTalk.innerHTML = item.cite;	
			review.appendChild(authorTalk);				
			}
		);
			

		/* Информация о книге */	 
		
		var bookDisplay = document.getElementById('book-display');
		
		var bookAndEye = document.createElement('div');
        bookAndEye.id = 'book-n-eye';
        bookDisplay.appendChild(bookAndEye);
    
        var bLargeCover = document.createElement('img');
		bLargeCover.className = 'book-cover-large';
		bLargeCover.src = parsedObject.cover.large;
		bLargeCover.title = parsedObject.title;
		bookAndEye.appendChild(bLargeCover);    
    
        var eye = document.createElement('div');
        eye.id = 'eye';
        bookAndEye.appendChild(eye);
    
        var apple = document.createElement('div');
        apple.id = 'apple';
        eye.appendChild(apple);
    
        var wink = document.createElement('div');
        wink.id = 'wink';
        eye.appendChild(wink);
		
		var bookDescription = document.createElement('p');
		bookDescription.className = 'book-description';
		bookDescription.innerHTML = parsedObject.description;	
		bookDisplay.appendChild(bookDescription);		
			
		var buyBook = document.createElement('div');
		buyBook.className = 'buy-book';
        if (windowSize > '640') {
            bookDisplay.appendChild(buyBook); //Вывод кнопки "Купить" внутри блока
        };
        
		var buttonToBuy = document.createElement('button');
		buttonToBuy.className = 'buy-book';
		buttonToBuy.type = 'submit';
		buttonToBuy.innerHTML = 'Купить за жалкие ' + parsedObject.price + ' Z';	
		buyBook.appendChild(buttonToBuy);
		
		var littleNote = document.createElement('p');
		littleNote.className = 'little-note';
		littleNote.innerHTML = 'Наши цены самые непредсказуемые и формируются нашим уборщиком Скраффи';	
		buyBook.appendChild(littleNote);
    
    
        /* Обработчик на кнопку "Купить", подтягивает код из файла order.html */
        buttonToBuy.addEventListener('click', function() {   
            var http = new XMLHttpRequest();  
            http.open('GET', '../pages/order.html');
            http.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementsByClassName('main-content')[0].innerHTML = this.responseText; 

                    var linkTitle = document.getElementById('backToBook');
                    linkTitle.innerHTML = parsedObject.title;
                    linkTitle.href = window.location;

                    var linkCover = document.getElementById('linkCover');
                    linkCover.src = parsedObject.cover.small;	
     
                    /* Вывод способов доставки */
       
                    var deliveries = document.getElementsByClassName('delivery')[0];                    
                    
                    for (i = 0; i < parsedDelivery.length; i++) {               
                        
                        var delivery = document.createElement('label');
                        delivery.className = 'radiobuttons';
                        deliveries.appendChild(delivery);

                        var deliveryInput = document.createElement('INPUT');
                        deliveryInput.name = "delivery";
                        deliveryInput.setAttribute("type", "radio");
                        deliveryInput.setAttribute("price", parsedDelivery[i].price);
                        deliveryInput.value = parsedDelivery[i].id;
                        deliveryInput.className = 'delivery-input';
                        delivery.appendChild(deliveryInput);

                        var deliveryInputName = document.createElement('span');
                        deliveryInputName.innerHTML = parsedDelivery[i].name + ' — ' + parsedDelivery[i].price + ' Z';
                        delivery.appendChild(deliveryInputName);     
                        
                    };
                    
                    document.getElementById('totalPrice').innerHTML = parsedObject.price;
                 
                    
                    /* Запрос и вывод способов оплаты */
                    var deliveryList = document.getElementsByClassName('delivery')[0];
                    deliveryList.addEventListener('change', function() {

                        var selectedDelivery = document.getElementsByClassName('delivery-input');    
                            for (i = 0; i < selectedDelivery.length; i++) {
                                
                                if (selectedDelivery[i].type === 'radio' && selectedDelivery[i].checked) {
                                    var gotDelivery = selectedDelivery[i].getAttribute('value');   
                                    var deliveryPrice = selectedDelivery[i].getAttribute('price');
                                    
                                    deliveryId = gotDelivery;
                                    
                                    var deliveryAddress = document.getElementsByName('address')[0].parentNode;
                                    if (gotDelivery == 'delivery-02' || gotDelivery == 'delivery-05') {
                                        var deliveryAddress = document.getElementsByName('address')[0].parentNode;
                                        deliveryAddress.classList.remove("hidden");
                                    } else {
                                        if (deliveryAddress.classList.contains("hidden") == 'false');
                                        deliveryAddress.classList.add("hidden");
                                    };
                                    
                                    makePaymentList(gotDelivery, deliveryPrice);
                                };
                            };                         
                    });
                    
                    var makePaymentList = function(paymentId, deliveryPrice) {
                        
                        var payments = document.getElementsByClassName('payment')[0];
                        
                        payments.innerHTML = '';
                        
                        var requestPayment = new XMLHttpRequest();

                        requestPayment.open('GET', 'https://netology-fbb-store-api.herokuapp.com/order/delivery/' + paymentId + '/payment');

                        requestPayment.onreadystatechange = function () {
                            if (this.readyState === 4) {
                                
                            parsedPayment = JSON.parse(this.responseText);
                                   
                            createList(parsedPayment);        
                          };
                        };
                        
                        requestPayment.send();
                        
                        
                        var createList = function(item) {
                            
                            var payments = document.getElementsByClassName('payment')[0];

                            var span = document.createElement('span');
                            span.innerHTML = 'Способ оплаты:<br>';    
                            payments.appendChild(span);    

                            for (i = 0; i < parsedPayment.length; i++) {               

                                var payment = document.createElement('label');
                                payment.className = 'radiobuttons';
                                payments.appendChild(payment);

                                var paymentInput = document.createElement('INPUT');
                                paymentInput.setAttribute("type", "radio");
                                paymentInput.name = "payment";
                                paymentInput.value = item[i].id;
                                paymentInput.className = 'payment-input';
                                payment.appendChild(paymentInput);

                                var paymentInputName = document.createElement('span');
                                paymentInputName.innerHTML = item[i].title;
                                payment.appendChild(paymentInputName);                        

                            };  
                        };  
                        document.getElementById('totalPrice').innerHTML = +parsedObject.price + +deliveryPrice; 
                    };
                    
                    /* Считывание выбранного способа оплаты */
                    var paymentList = document.getElementsByClassName('payment')[0];
                    paymentList.addEventListener('change', function() {

                        var selectedPayment = document.getElementsByClassName('payment-input');    
                            for (i = 0; i < selectedPayment.length; i++) {
                                
                                if (selectedPayment[i].type === 'radio' && selectedPayment[i].checked) {
                                    var gotPayment = selectedPayment[i].getAttribute('value');   
                                    
                                    paymentId = gotPayment;                                    
                                };
                            };                         
                    });                    
                                             
                    

                            /* Отправка формы. Валидация: */
                            var order = document.getElementsByClassName('order-it')[0];                                       
                    
                            order.addEventListener('click', function(event) {   
                                
                                var name = document.getElementsByName("name")[0];
                                var nameValue = document.getElementsByName("name")[0].value;                            
                                var phone = document.getElementsByName("phone")[0];
                                var phoneValue = document.getElementsByName("phone")[0].value;
                                var email = document.getElementsByName("email")[0];
                                var emailValue = document.getElementsByName("email")[0].value;
                                var comment = document.getElementsByName("comment")[0];
                                var commentValue = document.getElementsByName("comment")[0].value;
                                var address = document.getElementsByName("address")[0];
                                var addressValue = document.getElementsByName("address")[0].value;
                                var total = document.getElementById("totalPrice").innerHTML;                                  
                                
                                event.preventDefault();                       
                               if (nameValue.length == 0){
                                  name.className = "error";
                                  name.placeholder = "Укажите имя, пожалуйста";
                                  return false;
                               };
                                
                               if (phoneValue.length == 0){
                                  phone.className = "error";
                                  phone.placeholder = "Укажите номер, пожалуйста";
                                  return false;
                               };
                                
                               if (emailValue.length == 0){
                                  email.className = "error";
                                  email.placeholder = "Укажите e-mail, пожалуйста";
                                  return false;
                               }; 
                                
                               if ((deliveryId == 'delivery-02' || deliveryId == 'delivery-05') && addressValue.length == 0){
                                  address.className = "error";
                                  address.placeholder = "Укажите адрес, пожалуйста";
                                  return false;
                               };                                 
  
                               if (commentValue.length == 0){
                                  comment.className = "error";
                                  comment.placeholder = "Прокомментируйте свой заказ как-нибудь. Нам нужно знать, что вы осознаёте, что делаете.";
                                  return false;
                               };                                
                                
                               if (deliveryId == undefined ){
                                  var deliveryError = document.querySelectorAll('p.field ~ p.error')[0];
                                  deliveryError.classList.remove("hidden");
                                  return false;  
                               } else {
                                   if (paymentId == undefined){
                                      var paymentError = document.querySelectorAll('p.comment ~ p.error')[0];
                                      paymentError.classList.remove("hidden");
                                      return false;  
                                   };                                     
                               };                                
                                
                                
                                /* Сбор всех данных в реквест и отправка на сервер */
                                var request = new XMLHttpRequest();
                                var body = ('book=' + parsedObject.id + 
                                            '&name=' + encodeURIComponent(nameValue) + 
                                            '&phone=' + encodeURIComponent(phoneValue) + 
                                            '&email=' + encodeURIComponent(emailValue) + 
                                            '&comment=' + encodeURIComponent(commentValue) + 
                                            '&deliveryId=' + deliveryId + 
                                            '&deliveryAddress=' + encodeURIComponent(addressValue) + 
                                            '&paymentId=' + paymentId + 
                                            '&paymentCurrency=' + 'R01589' + 
                                            '&manager=' + 'wizzzjer%40gmail.com');    

                                request.open('POST', 'https://netology-fbb-store-api.herokuapp.com/order');
                                request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

                                request.onreadystatechange = function () {
                                    var orderForm = document.getElementsByClassName('order')[0];
                                    if (this.readyState === 4 && this.status == 200) {
                                        console.log('Status:', request.status);
                                        console.log('Headers:', request.getAllResponseHeaders());
                                        console.log('Body:', request.responseText);                                        
                                        orderForm.innerHTML = '<p class="sapient-description report">Заказ на книгу успешно оформлен.</p><p class="sapient-description">Спасибо, что спасли книгу от сжигания в печи.</p>';
                                    } else {
                                        orderForm.innerHTML = '<p class="sapient-description report">Что-то пошло не так.</p>';
                                    };
                                };

                                request.send(body);                
                            });    
                    
                    
                    
                };
            };
            http.send(null);
        });
                
                
		/* Особенности - features */
		var features = document.getElementsByClassName('features')[0]; 

		parsedObject.features.forEach(function(item) {
			var feature = document.createElement('figure');
			feature.className = 'feature';
			features.appendChild(feature);

			var featureFace = document.createElement('img');
			featureFace.src = item.pic;
			feature.appendChild(featureFace);				
			
			var featureSubj = document.createElement('figcaption');
			featureSubj.innerHTML = item.title;	
			feature.appendChild(featureSubj);				
        });
    
        if (windowSize <= '640') {
            bookStore.appendChild(buyBook); //Вывод кнопки "Купить" после всех блоков, если разрешение маленькое
        };
        
};




